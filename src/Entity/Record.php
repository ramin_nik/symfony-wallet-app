<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\RecordRepository;
use App\ServiceInterface\CreateRecordOwnerAndTimeInterface;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use App\Dto\RecordOutput;

/**
 * @ApiResource(attributes={"security"="is_granted('ROLE_USER') or is_granted('ROLE_ADMIN')"},
 *     output=RecordOutput::class,
 *     collectionOperations={"get","post"},
 *     itemOperations={"get","put",
 *     "delete"={"security"= "object.getOwner() == user or is_granted('ROLE_ADMIN')"
 *              }
 *     },
 *     normalizationContext={"groups"={"read_record"}},
 *     denormalizationContext={"groups"={"write_record"}})
 * @ORM\Entity(repositoryClass=RecordRepository::class)
 * @ApiFilter(SearchFilter::class, properties={"wallet.id" : "exact"})
 */
class Record implements CreateRecordOwnerAndTimeInterface
{
    /**
     * @ORM\Id
     * @Groups({"read_record","read_wallet"})
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @Groups({"read_record","write_record"})
     * @ORM\Column(type="string", length=255)
     */
    private $title;

    /**
     * @Groups({"read_record","write_record"})
     * @ORM\Column(type="smallint")
     */
    private $type;

    /**
     * @Groups({"read_record","write_record"})
     * @ORM\Column(type="integer")
     */
    private $amount;

    /**
     * @Groups({"read_record"})
     * @ORM\Column(type="datetime")
     */
    private $createdAt;

    /**
     * @Groups({"read_record"})
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="records")
     */
    private $owner;

    /**
     * @Groups({"read_record","write_record"})
     * @ORM\ManyToOne(targetEntity=Wallet::class, inversedBy="records")
     * @ORM\JoinColumn(nullable=false)
     */
    private $wallet;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getType(): ?int
    {
        return $this->type;
    }

    public function setType(int $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getAmount(): ?int
    {
        return $this->amount;
    }

    public function setAmount(int $amount): self
    {
        $this->amount = $amount;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getOwner(): ?User
    {
        return $this->owner;
    }

    public function setOwner(?User $owner): self
    {
        $this->owner = $owner;

        return $this;
    }

    public function getWallet(): ?Wallet
    {
        return $this->wallet;
    }

    public function setWallet(?Wallet $wallet): self
    {
        $this->wallet = $wallet;

        return $this;
    }
}
