<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\WalletRepository;
use App\ServiceInterface\CreateOwnerInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use App\Controller\api\GetEachWalletBalance;



/**
 * @ApiResource(attributes={"security"="is_granted('ROLE_USER') or is_granted('ROLE_ADMIN')"},
 *     collectionOperations={"get_balance"={
 *         "method"="Get",
 *         "path"="/wallet/getBalanceEachWallet",
 *         "controller"=GetEachWalletBalance::class,
 *             "openapi_context": {
 *                 "responses": {
 *                     "200":{
 *                       "content": {
 *                         "application/json": {
 *                             "schema": {
 *                                 "type": "object",
 *                                 "example": {
 *                                         "wallet_name":100
 *                                },
 *                             },
 *                         },
 *                       },
 *                    }
 *                 },
 *             },
 *     },
 *     "get","post"},
 *     itemOperations={"get","put","delete"},
 *     normalizationContext={"groups"={"read_wallet"}},
 *     denormalizationContext={"groups"={"write_wallet"}})
 * @ORM\Entity(repositoryClass=WalletRepository::class)
 */
class Wallet implements CreateOwnerInterface
{
    /**
     * @Groups({"read_wallet","read_record"})
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @Groups({"read_wallet","write_wallet","read_user"})
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @Groups({"read_wallet","write_wallet","read_user"})
     * @ORM\Column(type="smallint")
     */
    private $type;

    /**
     * @Groups({"read_wallet"})
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="wallets")
     * @ORM\JoinColumn(nullable=false)
     */
    private $owner;

    /**
     * @ORM\OneToMany(targetEntity=Record::class, mappedBy="wallet", orphanRemoval=true)
     */
    private $records;

    public function __construct()
    {
        $this->records = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getType(): ?int
    {
        return $this->type;
    }

    public function setType(int $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getOwner(): ?user
    {
        return $this->owner;
    }

    public function setOwner(?user $owner): self
    {
        $this->owner = $owner;

        return $this;
    }

    /**
     * @return Collection|Record[]
     */
    public function getRecords(): Collection
    {
        return $this->records;
    }

    public function addRecord(Record $record): self
    {
        if (!$this->records->contains($record)) {
            $this->records[] = $record;
            $record->setWallet($this);
        }

        return $this;
    }

    public function removeRecord(Record $record): self
    {
        if ($this->records->removeElement($record)) {
            // set the owning side to null (unless already changed)
            if ($record->getWallet() === $this) {
                $record->setWallet(null);
            }
        }

        return $this;
    }
}
