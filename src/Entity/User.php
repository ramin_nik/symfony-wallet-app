<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\UserRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Serializer\Annotation\Groups;
use App\Controller\api\GetBalance;

/**
 * @ApiResource(
 *      normalizationContext={"groups"={"read_user"}},
 *     collectionOperations={
 *     "get_balance"={
 *         "method"="Get",
 *         "path"="/user/getBalance",
 *         "controller"=GetBalance::class,
 *         "security"= "is_granted('ROLE_USER')",
 *             "openapi_context": {
 *                 "responses": {
 *                     "200":{
 *                       "content": {
 *                         "application/json": {
 *                             "schema": {
 *                                 "type": "object",
 *                                 "properties": {
 *                                     "balance": {"type": "string", "example": "1000$"},
 *                                 },
 *                             },
 *                         },
 *                       },
 *                    }
 *                 },
 *             },
 *     },
 *     "get" = {"security" = "is_granted('ROLE_ADMIN')"},
 *     "post"={"denormalization_context"={"groups"={"write_user"}}}
 * },
 *   itemOperations={
 *          "get"={"security" = "is_granted('ROLE_ADMIN')"}
 *               ,
 *          "put"={"security"= "is_granted('ROLE_ADMIN')",
 *                  "denormalization_context" = {"groups"={"put_user"}}
 *                },
 *           "delete"={"security" = "object.getEmail() == user or is_granted('ROLE_ADMIN')"
 *                }
 *       },
 *   )
 * @ORM\Entity(repositoryClass=UserRepository::class)
 * @UniqueEntity(fields={"email"}, message="There is already an account with this email")
 */
class User implements UserInterface
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @Groups({"read_user","read_wallet","read_record"})
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=180, unique=true)
     * @Groups({"read_user","write_user","put_user","read_wallet","read_record"})
     */
    private $email;

    /**
     * @Groups({"read_user","put_user"})
     * @ORM\Column(type="json")
     */
    private $roles = [];

    /**
     * @var string The hashed password
     * @ORM\Column(type="string")
     * @Groups({"write_user","put_user"})
     */
    private $password;

    /**
     * @ORM\OneToMany(targetEntity=Wallet::class, mappedBy="owner", orphanRemoval=true)
     */
    private $wallets;

    /**
     * @ORM\OneToMany(targetEntity=Record::class, mappedBy="owner")
     */
    private $records;

    public function __construct()
    {
        $this->wallets = new ArrayCollection();
        $this->records = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    /**
     * A visual identifier that represents this user.
     *
     * @see UserInterface
     */
    public function getUsername(): string
    {
        return (string)$this->email;
    }

    /**
     * @see UserInterface
     */
    public function getRoles(): array
    {
        $roles = $this->roles;
        // guarantee every user at least has ROLE_USER
        $roles[] = 'ROLE_USER';

        return array_unique($roles);
    }

    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getPassword(): string
    {
        return (string)$this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getSalt()
    {
        // not needed when using the "bcrypt" algorithm in security.yaml
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials()
    {
        // If you store any temporary, sensitive data on the user, clear it here
        // $this->plainPassword = null;
    }

    /**
     * @return Collection|Wallet[]
     */
    public function getWallets(): Collection
    {
        return $this->wallets;
    }

    public function addWallet(Wallet $wallet): self
    {
        if (!$this->wallets->contains($wallet)) {
            $this->wallets[] = $wallet;
            $wallet->setOwner($this);
        }

        return $this;
    }

    public function removeWallet(Wallet $wallet): self
    {
        if ($this->wallets->removeElement($wallet)) {
            // set the owning side to null (unless already changed)
            if ($wallet->getOwner() === $this) {
                $wallet->setOwner(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Record[]
     */
    public function getRecords(): Collection
    {
        return $this->records;
    }

    public function addRecord(Record $record): self
    {
        if (!$this->records->contains($record)) {
            $this->records[] = $record;
            $record->setOwner($this);
        }

        return $this;
    }

    public function removeRecord(Record $record): self
    {
        if ($this->records->removeElement($record)) {
            // set the owning side to null (unless already changed)
            if ($record->getOwner() === $this) {
                $record->setOwner(null);
            }
        }

        return $this;
    }
}
