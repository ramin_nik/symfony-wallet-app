<?php


namespace App\Doctrine;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Extension\QueryCollectionExtensionInterface;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Extension\QueryItemExtensionInterface;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Util\QueryNameGeneratorInterface;
use App\Entity\Record;
use App\Entity\Wallet;
use Doctrine\ORM\QueryBuilder;
use Symfony\Component\Security\Core\Security;


class CurrentUserExtension implements QueryCollectionExtensionInterface, QueryItemExtensionInterface
{
    private $security;

    public function __construct(Security $security)
    {
        $this->security = $security;
    }
    public function applyToCollection(QueryBuilder $queryBuilder, QueryNameGeneratorInterface $queryNameGenerator, string $resourceClass, string $operationName = null): void
{
    $this->addWhere($queryBuilder, $resourceClass);
}

    public function applyToItem(QueryBuilder $queryBuilder, QueryNameGeneratorInterface $queryNameGenerator, string $resourceClass, array $identifiers, string $operationName = null, array $context = []): void
{
    $this->addWhere($queryBuilder, $resourceClass);
}

    private function addWhere(QueryBuilder $queryBuilder, string $resourceClass): void
{
    $user = $this->security->getUser() ;
    if($user!==null){
        if (Wallet::class === $resourceClass ) {
            $rootAlias = $queryBuilder->getRootAliases()[0];
            $queryBuilder->andWhere(sprintf($rootAlias.'.owner = :current_user'));
            $queryBuilder->setParameter('current_user',$user->getId());
        }
        if (Record::class === $resourceClass ) {
            $rootAlias = $queryBuilder->getRootAliases()[0];
            $queryBuilder->andWhere(sprintf($rootAlias.'.owner = :current_user'));
            $queryBuilder->setParameter('current_user',$user->getId());

        }
    }else{
        return;
    }




}
}