<?php

namespace App\DataTransformer;


use ApiPlatform\Core\DataTransformer\DataTransformerInterface;
use App\Dto\RecordOutput;
use App\Entity\Record;

class RecordOutputDataTransformer implements DataTransformerInterface
{
    /**
     * {@inheritdoc}
     */
    public function transform($data, string $to, array $context = [])
    {
        $record= new RecordOutput();
        $record->setId($data->getId());
        $record->setName($data->getTitle());
        if($data->getType()==1){
            $record->setVolume($data->getAmount()."$");

        }else{
            $record->setVolume("-".$data->getAmount()."$");
        }
        $record->setOwner($data->getOwner()->getEmail());
        $record->setWalletName($data->getWallet()->getName());
        $record->setWalletId($data->getWallet()->getId());

        return $record;
    }

    /**
     * {@inheritdoc}
     */
    public function supportsTransformation($data, string $to, array $context = []): bool
    {

        return RecordOutput::class === $to && $data instanceof Record;
    }

}