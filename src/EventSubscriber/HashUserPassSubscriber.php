<?php


namespace App\EventSubscriber;
use ApiPlatform\Core\EventListener\EventPriorities;
use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Event\ViewEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class HashUserPassSubscriber implements EventSubscriberInterface
{
    /**
     * @var EntityManagerInterface $manager
     */
    private EntityManagerInterface $manger;
    /**
     * @var UserPasswordEncoderInterface
     */
    private UserPasswordEncoderInterface $passwordEncoder;

    public function __construct(EntityManagerInterface $manager, UserPasswordEncoderInterface  $passwordEncoder)
    {
        $this->manger=$manager;
        $this->passwordEncoder = $passwordEncoder;
    }

    public static function getSubscribedEvents()
    {
        return [
            KernelEvents::VIEW => ['hashPass', EventPriorities::PRE_WRITE],
        ];
    }

    public function hashPass(ViewEvent $event): void
    {
        $entity = $event->getControllerResult();
        $method = $event->getRequest()->getMethod();
        if ($entity instanceof User && Request::METHOD_POST === $method || Request::METHOD_PUT === $method) {
            $encoded= $this->passwordEncoder->encodePassword($entity,$entity->getPassword());
            $entity->setPassword($encoded);
        }


    }
}