<?php


namespace App\EventSubscriber;
use ApiPlatform\Core\EventListener\EventPriorities;
use App\Entity\User;
use App\ServiceInterface\CreateOwnerInterface;
use App\ServiceInterface\CreateRecordOwnerAndTimeInterface;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Event\ViewEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\Security\Core\Security;

class CreateOwnerSubscriber implements EventSubscriberInterface
{
    /**
     * @var EntityManagerInterface $manager
     */
    private EntityManagerInterface $manger;
    /**
     * @var Security
     */
    private Security $security;

    public function __construct(EntityManagerInterface $manager, Security $security)
    {
        $this->manger=$manager;
        $this->security = $security;
    }

    public static function getSubscribedEvents()
    {
        return [
            KernelEvents::VIEW => ['createOwner', EventPriorities::PRE_WRITE],
        ];
    }

    public function createOwner(ViewEvent $event): void
    {
        $entity = $event->getControllerResult();
        $method = $event->getRequest()->getMethod();
        if ($entity instanceof CreateOwnerInterface && Request::METHOD_POST === $method) {
            $user=$this->security->getUser();
            $entity->setOwner($user);
        }
        if ($entity instanceof CreateRecordOwnerAndTimeInterface && Request::METHOD_POST === $method) {
            $user=$this->security->getUser();
            $entity->setOwner($user);
            $entity->setCreatedAt(new \DateTime('now'));
        }

    }
}