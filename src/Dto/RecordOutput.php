<?php


namespace App\Dto;


use Symfony\Component\Serializer\Annotation\Groups;

class RecordOutput
{
    private $id;
    private $name;
    private $volume;
    private $owner;
    private $walletName;
    private $walletId;

    /**
     * @param mixed $walletName
     */
    public function setWalletName($walletName): void
    {
        $this->walletName = $walletName;
    }

    /**
     * @return mixed
     * @Groups({"read_record"})
     */
    public function getWalletName()
    {
        return $this->walletName;
    }

    /**
     * @param mixed $walletId
     */
    public function setWalletId($walletId): void
    {
        $this->walletId = $walletId;
    }

    /**
     * @return mixed
     * @Groups({"read_record"})
     */
    public function getWalletId()
    {
        return $this->walletId;
    }

    /**
     * @param mixed $owner
     */
    public function setOwner($owner): void
    {
        $this->owner = $owner;
    }

    /**
     * @return mixed
     * @Groups({"read_record"})
     */
    public function getOwner()
    {
        return $this->owner;
    }

    /**
     * @param mixed $id
     * @Groups({"read_record"})
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     * @Groups({"read_record"})
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name): void
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     * @Groups({"read_record"})
     */
    public function getVolume()
    {
        return $this->volume;
    }

    /**
     * @param mixed $description
     */
    public function setVolume($volume): void
    {
        $this->volume = $volume;
    }

}