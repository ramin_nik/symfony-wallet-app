<?php

namespace App\Repository;

use App\Entity\Record;
use App\Entity\User;
use App\Entity\Wallet;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Record|null find($id, $lockMode = null, $lockVersion = null)
 * @method Record|null findOneBy(array $criteria, array $orderBy = null)
 * @method Record[]    findAll()
 * @method Record[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class RecordRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Record::class);
    }

     /**
      * * @param User $user
      * @return Record[] Returns an array of Record objects
      */

    public function findByUser(User $user): array
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.owner = :val')
            ->setParameter('val', $user->getId())
            ->orderBy('r.id', 'ASC')
            ->getQuery()
            ->getResult()
        ;
    }
    /**
     * @param Record[] $records
     * @return Record[] Returns an array of Record objects
     */

    public function groupByWallet(array $records): array
    {
        $groupedByWallet = [];
        foreach ($records as $rercord) {
            $groupedByWallet[$rercord->getWallet()->getName()] [] = $rercord;
        }
        return $groupedByWallet;

    }
    /**
     * @param Record[] $records
     * @return Record[] Returns an array of Record objects
     */

    public function sumAmountPerWallet(array $records): array
    {
        $recordsWithSum = [];
        foreach ($records as $wallet => $recordInWallet) {
            $recordsWithSum[$wallet] []= $this->sumAmounts($recordInWallet);
        }
        return $recordsWithSum;

    }

    /**
     * @param Record[] $records
     */
    public function sumAmounts(array $records){
        $sum = 0;
        foreach ($records as $record){
            $amount = intval($record->getAmount());
            $type= $record->getType();
            if($type==2){$amount = -1* abs($amount);}
           $sum +=$amount;

        }
        return $sum;
    }
    /*
    public function findOneBySomeField($value): ?Record
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
