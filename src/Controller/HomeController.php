<?php

namespace App\Controller;

use App\Entity\Record;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class HomeController extends AbstractController
{
    /**
     * @Route("/app/home", name="app_home")
     */
    public function index(): Response
    {
        $records = $this->getDoctrine()->getRepository(Record::class)->findByUser($this->getUser());
        $sumAmount=$this->getDoctrine()->getRepository(Record::class)->sumAmounts($records);

        return $this->render('home/index.html.twig', [
            'controller_name' => 'HomeController',
            'balance'=> $sumAmount

        ]);
    }
}
