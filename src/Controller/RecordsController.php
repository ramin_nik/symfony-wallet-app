<?php

namespace App\Controller;

use App\Entity\Record;
use App\Entity\Wallet;
use Doctrine\ORM\EntityNotFoundException;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class RecordsController extends AbstractController
{
    /**
     * @Route("/app/records", name="app_records")
     */
    public function index(): Response
    {
        $records = $this->getDoctrine()->getRepository(Record::class)->findByUser($this->getUser());
//        $recordsByWallet = $this->getDoctrine()->getRepository(Record::class)->groupByWallet($records);
           $sumAmount=$this->getDoctrine()->getRepository(Record::class)->sumAmounts($records);
        return $this->render('records/index.html.twig', [
            'controller_name' => 'RecordsController',
            'deleteConfirmMessage'=>'Are you sure?',
            'records'=>$records,
            'sumAmount'=>$sumAmount
        ]);
    }

    /**
     * @Route("/app/records/create", name="app_records_create", methods={"GET","POST"})
     */
    public function create(Request $request): Response
    {
        if ($request->getMethod() === "POST") {
            $record = new Record();
            $record->setTitle($request->request->get('title'));
            $record->setAmount($request->request->get('amount'));
            $record->setOwner($this->getUser());
            $record->setCreatedAt(new \DateTime('now'));
            $record->setWallet($this->getDoctrine()->getRepository(Wallet::class)->find($request->request->get('wallet')));
            $record->setType(intval($request->request->get('type')));
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($record);
            $entityManager->flush();
            return $this->redirect($this->generateUrl('app_records'));
        } else {
            $wallets = $this->getDoctrine()->getRepository(Wallet::class)->findByUser($this->getUser());
            return $this->render('records/create.html.twig',
            ['wallets'=>$wallets]);
        }

    }

    /**
     * @Route("/app/record/delete/{recordId}",name="app_record_delete",methods={"GET","POST"})
     *
     * @return Response
     */
    public function delete(Request $request)
    {
        $record = $this->getDoctrine()->getRepository(Record::class)->find($request->get('recordId'));
        if ($record){
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($record);
            $entityManager->flush();
        }else{
            throw new EntityNotFoundException('Please check if you have requested the correct action!');
        }
        return new RedirectResponse($this->generateUrl('app_records'));
    }
}
