<?php

namespace App\Controller;

use App\Entity\Wallet;
use Doctrine\ORM\EntityNotFoundException;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class WalletsController extends AbstractController
{
    /**
     * @Route("/app/wallets", name="app_wallets", methods={"GET"})
     */
    public function index(): Response
    {
        $wallets = $this->getDoctrine()->getRepository(Wallet::class)->findByUser($this->getUser());
        return $this->render('wallets/index.html.twig', [
            'controller_name' => 'Wallets',
            'deleteConfirmMessage'=>'Are you sure?',
            'wallets'=>$wallets
        ]);
    }

    /**
     * @Route("/app/wallets/create", name="app_wallets_create", methods={"GET","POST"})
     */
    public function create(Request $request): Response
    {
        if ($request->getMethod() === "POST") {
            $wallet = new Wallet();
            $wallet->setName($request->request->get('name'));
            $wallet->setType(intval($request->request->get('type')));
            $wallet->setOwner($this->getUser());
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($wallet);
            $entityManager->flush();
            return $this->redirect($this->generateUrl('app_wallets'));
        } else {

            return $this->render('wallets/create.html.twig');
        }

    }
    /**
     * @Route("/app/wallet/delete/{walletId}",name="app_wallet_delete",methods={"GET","POST"})
     *
     * @return Response
     */
    public function delete(Request $request)
    {
        $wallet = $this->getDoctrine()->getRepository(Wallet::class)->find($request->get('walletId'));
        if ($wallet){
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($wallet);
            $entityManager->flush();
        }else{
            throw new EntityNotFoundException('Please check if you have requested the correct action!');
        }
        return new RedirectResponse($this->generateUrl('app_wallets'));
    }
}
