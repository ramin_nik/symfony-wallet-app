<?php

namespace App\Controller;

use App\Entity\Record;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ReportController extends AbstractController
{
    /**
     * @Route("/app/report", name="app_report")
     */
    public function index(): Response
    {
        $records = $this->getDoctrine()->getRepository(Record::class)->findByUser($this->getUser());
        $recordsByWallet = $this->getDoctrine()->getRepository(Record::class)->groupByWallet($records);
        $balanceEachWallet=$this->getDoctrine()->getRepository(Record::class)->sumAmountPerWallet($recordsByWallet);
        $sumAmount=$this->getDoctrine()->getRepository(Record::class)->sumAmounts($records);

        return $this->render('report/index.html.twig', [
            'walletBalances'=>$balanceEachWallet,
            'walletRecords'=>$recordsByWallet,
            'fullBalance'=>$sumAmount,
            'controller_name' => 'ReportController',
        ]);
    }
}
