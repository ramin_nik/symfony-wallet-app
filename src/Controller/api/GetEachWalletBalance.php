<?php


namespace App\Controller\api;


use App\Entity\Record;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Security\Core\Security;

class GetEachWalletBalance
{
    /**
     * @var EntityManagerInterface
     */
    private EntityManagerInterface $entityManager;
    /**
     * @var Security
     */
    private Security $security;

    public function __construct(EntityManagerInterface $entityManager,Security $security)
    {
        $this->entityManager = $entityManager;
        $this->security = $security;
    }

    public function __invoke(): JsonResponse
    {
        $records = $this->entityManager->getRepository(Record::class)->findByUser($this->security->getUser());
        $recordsByWallet = $this->entityManager->getRepository(Record::class)->groupByWallet($records);
        $balanceEachWallet=$this->entityManager->getRepository(Record::class)->sumAmountPerWallet($recordsByWallet);
        return new JsonResponse($balanceEachWallet);
    }
}