<?php


namespace App\Controller\api;


use App\Entity\Record;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Security\Core\Security;

class GetBalance
{

    /**
     * @var EntityManagerInterface
     */
    private EntityManagerInterface $entityManager;
    /**
     * @var Security
     */
    private Security $security;

    public function __construct(EntityManagerInterface $entityManager,Security $security)
    {
        $this->entityManager = $entityManager;
        $this->security = $security;
    }

    public function __invoke()
    {
        $records = $this->entityManager->getRepository(Record::class)->findByUser($this->security->getUser());
        $sumAmount=$this->entityManager->getRepository(Record::class)->sumAmounts($records);
        return new JsonResponse(["balance"=>$sumAmount."$"]);
    }

}