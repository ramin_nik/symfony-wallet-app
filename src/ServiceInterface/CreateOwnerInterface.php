<?php


namespace App\ServiceInterface;


use App\Entity\User;

interface CreateOwnerInterface
{
    public function setOwner(?user $owner): self;
    public function getOwner(): ?user;

}