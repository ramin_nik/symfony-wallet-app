<?php


namespace App\ServiceInterface;


use App\Entity\User;

interface CreateRecordOwnerAndTimeInterface
{
    public function setOwner(?user $owner): self;
    public function getOwner(): ?user;
    public function setCreatedAt(\DateTimeInterface $createdAt): self;
    public function getCreatedAt(): ?\DateTimeInterface;


}